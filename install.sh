

virtualenv webapp
source webapp/bin/activate
echo "Installing dependencies"
pip install -r requirements.txt 

echo "Installing ISR"
git clone https://github.com/idealo/image-super-resolution
cd image-super-resolution
python setup.py install
cd ../

echo "Unzipping Blender-2.92"
cd webapp
tar -xf blender-2.92.0-linux64.tar.xz

# activate executable 
cd ../
chmod +x texture_generator
 
echo "Completed."
echo "Command to activate virtual environment: source webapp/source/bin/activate"
