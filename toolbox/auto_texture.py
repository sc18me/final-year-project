import bpy
import os


def blender_rm_all():
    bpy.data.batch_remove(bpy.data.objects)
    bpy.data.batch_remove(bpy.data.cameras)
    bpy.data.batch_remove(bpy.data.curves)
    bpy.data.batch_remove(bpy.data.images)
    bpy.data.batch_remove(bpy.data.textures)
    bpy.data.batch_remove(bpy.data.meshes)
    bpy.data.batch_remove(bpy.data.images)


def bind_texture(model_fp, texture_fp, dest_fp):
    bpy.data.batch_remove(bpy.data.objects)
    bpy.ops.import_scene.obj(filepath=model_fp)

    # apply texture to material
    mat = bpy.data.materials.new(name="new_mat")
    mat.use_nodes = True
    bsdf = mat.node_tree.nodes["Principled BSDF"]
    texImage = mat.node_tree.nodes.new('ShaderNodeTexImage')
    texImage.image = bpy.data.images.load(texture_fp)
    mat.node_tree.links.new(bsdf.inputs['Base Color'], texImage.outputs['Color'])

    bpy.ops.object.select_all(action='SELECT')

    # need to activate each obj and apply textures separately
    for obj in bpy.data.objects:
        bpy.context.view_layer.objects.active = obj
        bpy.ops.object.mode_set(mode="EDIT")
        if obj.data.materials:
            obj.data.materials[0] = mat
        else:
            obj.data.materials.append(mat)

    bpy.ops.export_scene.obj(filepath=dest_fp)


blender_rm_all()
bind_texture("/home/micah/year3/dissertation/datasets/3D-FUTURE-model/3D-FUTURE-model/0a1d56a4-2fce-4c35-bba1-bf6127457b4c/normalized_model.obj",
"/home/micah/year3/dissertation/datasets/3D-FUTURE-model/3D-FUTURE-model/0a1d56a4-2fce-4c35-bba1-bf6127457b4c/texture.png",
             "/home/micah/model.obj")