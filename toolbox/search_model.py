import json
import os

DATA_ROOT = "/home/micah/year3/dissertation/datasets/3D-FUTURE-model"
METADATA = os.path.join(DATA_ROOT, "model_info.json")

# the exact conditions used to filter the dataset the map was in. find the file used to extract dataset for conditions
conditions = {"Cabinet/Shelf/Desk": None, "Table": None, "Chair": None}

starting_idx = 0
map_index = 2


with open(METADATA) as json_file:
    data = json.load(json_file)
    idx = starting_idx
    for model in data:
        if model['super-category'] in conditions:
            if idx == map_index:
                print(os.path.join(DATA_ROOT, "3D-FUTURE-model", model["model_id"], "normalized_model.obj"))
            idx += 1
