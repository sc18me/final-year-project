from PIL import Image, ImageFile
import os
import shutil
import json
from enum import Enum
# from data_understanding import categories
import numpy as np
import numpy.linalg as la
from sklearn.neighbors import KDTree
from joblib import Parallel, delayed
import multiprocessing
import tifffile as tf
import cv2

UPLOAD_TEXTURES_LOC = "app/uploads/textures"

ImageFile.LOAD_TRUNCATED_IMAGES = True

DATA_ROOT = "/home/micah/year3/dissertation/datasets/3D-FUTURE-model"
METADATA = os.path.join(DATA_ROOT, "model_info.json")
BLANK_DATASET = os.path.join(DATA_ROOT, "blank-uv-maps")
materials = {"Wood": (204, 102, 0), "Smooth Leather": (204, 255, 255),
             "Rough Cloth": (255,255,153), "Composite Board": (255,0,0)}

BLANK_IMG_WIDTH = 256
STATIC_LOC = "app/static"


class MaterialPainter:

    def __init__(self, csv_fp):
        # ALL OF THESE MUST BE RESET WITH EACH OBJ!!
        self.coord_height = None
        self.points = None

        # fields for linear interpolation
        self.max_vx = None
        self.min_vx = None
        self.min_vy = None
        self.max_vy = None
        self.min_vz = None
        self.max_vz = None

        self.tree = None
        self.csv_fp = csv_fp
        self.pixel2vertex = None

        self.v_orientation_patch = cv2.imread(f"{STATIC_LOC}{os.path.sep}v_orientation.png")
        self.h_orientation_patch = cv2.imread(f"{STATIC_LOC}{os.path.sep}h_orientation.png")
        self.h_orientation_patch = cv2.cvtColor(self.h_orientation_patch, cv2.COLOR_BGRA2GRAY)
        self.v_orientation_patch = cv2.cvtColor(self.v_orientation_patch, cv2.COLOR_BGRA2GRAY)
        # wxw patch
        self.patch_length = self.v_orientation_patch.shape[0]

    def normalized_vx(self, vx):
        "normalizes height between [0-255)"
        return (vx - self.min_vx) / (self.max_vx - self.min_vx) * 255

    def normalized_vy(self, vy):
        "normalizes height between [0-255)"
        return (vy - self.min_vy) / (self.max_vy - self.min_vy) * 255

    def normalized_vz(self, vz):
        "normalizes height between [0-255)"
        return (vz - self.min_vz) / (self.max_vz - self.min_vz) * 255

    def resest_state(self):
        self.max_vx = -np.inf
        self.min_vx = np.inf
        self.max_vy = -np.inf
        self.min_vy = np.inf
        self.max_vz = -np.inf
        self.min_vz = np.inf

        self.coord_height = {}

        self.pixel2vertex = {}

    def read_obj_data(self):
        with open(self.csv_fp, "r") as f:
            f.readline()
            line = f.readline()
            count = 0

            while line != '':
                count += 1
                line = f.readline()

            if count == 0:
                return False

            self.points = np.zeros((count, 2))
            self.resest_state()

            f.seek(0)
            # skip fields
            f.readline()

            line = f.readline()

            idx = 0
            while line != '':
                line = line.replace("\n", "").split(",")

                vx = float(line[0])
                vy = float(line[1])
                vz = float(line[2])

                pixel_x = int(line[3])
                pixel_y = int(line[4])

                if vx > self.max_vx:
                    self.max_vx = vx

                if vx < self.min_vx:
                    self.min_vx = vx

                if vy > self.max_vy:
                    self.max_vy = vy

                if vy < self.min_vy:
                    self.min_vy = vy

                if vz > self.max_vz:
                    self.max_vz = vz

                if vz < self.min_vz:
                    self.min_vz = vz

                # store x,y pos of pixels
                point = np.array([pixel_x, pixel_y])
                self.points[idx] = point
                self.pixel2vertex[pixel_x, pixel_y] = vx, vy, vz

                line = f.readline()
                idx += 1

            self.tree = KDTree(self.points)
        f.close()
        return True

    @staticmethod
    def convert_to_tiff_n(src, dest, n_ch):
        """converts an image to a tiff file with n channels"""

        im = Image.open(src).convert("RGB")
        im = MaterialPainter.scale_img(BLANK_IMG_WIDTH, im)

        w, h = im.size
        padding = np.zeros((256, 256, n_ch))
        padding[:h, :w, :3] = np.asarray(im)

        tf.imwrite(dest, padding)

    def extract_edges(self, src_img) -> Image:
        """ extracts edges within the image. Used as a feature to describe texture orientation

            returns: a 256x256 image matrix containing edges of texture
        """
        im = Image.open(src_img).convert("RGB")
        im = MaterialPainter.scale_img(256, im)
        img = np.asarray(im)
        # pick up stronger texture details
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        edges = cv2.Canny(img, 30, 60)
        # invert image
        edges = cv2.bitwise_not(edges)

        return Image.fromarray(edges)

    @staticmethod
    def draw_outline(src_img):
        """
        params:
            src_img - path to source image

        returns: RGB image
        """

        im_draw = Image.open(src_img).convert("RGBA")
        im_ref = Image.open(src_img).convert("RGBA")
        arr = np.asarray(im_draw)

        pixel_map = im_draw.load()
        w, h = im_draw.size

        stack = []

        for y in range(h):
            for x in range(w):
                if x == 0:
                    stack.clear()

                if im_draw.getpixel((x, y))[3] > 0:
                    # it is part of the outline
                    if len(stack) == 0:
                        stack.append(1)
                        pixel_map[x, y] = 0, 0, 0

                    else:
                        pixel_map[x, y] = 255, 255, 255, 255

                else:
                    # we have reached the end of the outline. paint previous pixel that was part of outline
                    if len(stack) > 0:
                        stack.clear()
                        pixel_map[x - 1, y] = 0, 0, 0

                    pixel_map[x, y] = 255, 255, 255, 255

        stack = []
        for x in range(w):
            for y in range(h):
                if y == 0:
                    stack.clear()

                if im_ref.getpixel((x, y))[3] > 0:
                    # it is part of the outline
                    if len(stack) == 0:
                        stack.append(1)
                        pixel_map[x, y] = 0, 0, 0

                else:
                    # we have reached the end of the outline. paint previous pixel that was part of outline
                    if len(stack) > 0:
                        stack.clear()
                        pixel_map[x, y - 1] = 0, 0, 0

        return cv2.cvtColor(np.array(im_draw), cv2.COLOR_BGRA2GRAY)

    def draw_edges(self, src_im, points, orientation):
        cv_im = self.draw_outline(src_im)
        if orientation == "none":
            return cv_im

        if orientation == "horizontal":
            for point in points:
                x = int(point[0])
                y = int(point[1])
                if cv_im[y:y + self.patch_length, x:x + self.patch_length].shape != (44,44):
                    continue
                cv_im[y:y + self.patch_length, x:x + self.patch_length] = self.h_orientation_patch
        else:
            pass

        return cv_im


    def make_mask7(self, src_img, dest_img, material, points, orientation, use_reference):
        """ labels a texture based on its placement on the model's geometry + target style + texture edges/orientation

            params:
                -src_img: path to image
                -orientation: vertical or horizontal
                -positions: list of (x,y) positions of patches orientation patches.

            saves: a .tiff image (256,256,7) containing mask of texture describing target material
                     AND xyz coordinates of the texture in model space AND an image describing texture edges


            returns: True/False on validity of example
        """

        im = Image.open(src_img).convert("RGBA")
        im = MaterialPainter.scale_img(BLANK_IMG_WIDTH, im)

        # A
        xyz = np.zeros((256, 256, 3))
        # pad out a (256, 256, 7) matrix. We will use this template to place our features
        padding = np.zeros((256, 256, 7))

        pixel_map = im.load()
        w, h = im.size
        if w != 256 or h != 256:
            return False

        for y in range(h):
            for x in range(w):

                # if im.getpixel((x, y))[0] > 0 or im.getpixel((x, y))[1] > 0 or im.getpixel((x, y))[2] > 0:
                if im.getpixel((x,y))[3] > 0:
                    if (x, y) in self.pixel2vertex:
                        pixel_map[x, y] = materials[material][0], materials[material][1], \
                                          materials[material][2]

                        # get vertex coords and fill in xyz matrix
                        # normalize points from 0-255
                        vx, vy, vz = self.pixel2vertex[x,y]
                        # construct point representing (relative) position in world coordinates for a given x,y position
                        # ... in pixel space
                        point = np.array([self.normalized_vx(vx), self.normalized_vy(vy), self.normalized_vz(vz)])
                        xyz[y,x] = point

                    else:
                        # fill any remaining gaps
                        pixel_map[x, y] = materials[material][0], materials[material][1], \
                                          materials[material][2]
                        nearest_pixel_x, nearest_pixel_y = self.nearest_neighbour(np.array([[x, y]]))
                        vx, vy, vz = self.pixel2vertex[nearest_pixel_x, nearest_pixel_y]
                        point = np.array([self.normalized_vx(vx), self.normalized_vy(vy), self.normalized_vz(vz)])
                        xyz[y,x] = point



        im.save("temp.png")
        im = Image.open("temp.png").convert("RGB")
        im_arr = np.asarray(im)

        # fill in model xyz data
        padding[:, :, 3:6] = xyz

        # fill in mask image
        padding[:, :, :3] = im_arr

        # # fill in edges image
        if use_reference:
            edges = np.asarray(self.extract_edges(UPLOAD_TEXTURES_LOC+"/ref_texture.png"))
            padding[:, :, 6] = edges
            Image.fromarray(edges).save("edges.png")
        else:
            edges = self.draw_edges(src_img, points, orientation)
            padding[:, :, 6] = edges

        Image.fromarray(edges).save("test_edges.png")

        tf.imwrite(dest_img, padding)
        return True


    def make_mask6(self, src_img, dest_img, material):
        """ labels a texture based on its placement on the model's geometry + target style

            saves: a .tiff image (256,256,6) containing mask of texture describing target material
                     AND xyz coordinates of the texture in model space

            returns: True/False on validity of example
        """

        # we need to detect anywhere that is not completely transparent and paint over it
        # RGBA will help us remove lines forming faces in UV map.
        im = Image.open(src_img).convert("RGBA")
        im = MaterialPainter.scale_img(BLANK_IMG_WIDTH, im)

        # A
        xyz = np.zeros((256, 256, 3))
        temp = np.zeros((256, 256, 6))

        pixel_map = im.load()
        w, h = im.size
        if w != 256 or h != 256:
            return False

        for y in range(h):
            for x in range(w):

                if im.getpixel((x, y))[3] > 0:
                    if (x, y) in self.pixel2vertex:
                        pixel_map[x, y] = materials[material][0], materials[material][1], \
                                          materials[material][2]

                        # get vertex coords and fill in xyz matrix
                        # normalize points from 0-255
                        vx, vy, vz = self.pixel2vertex[x,y]
                        # construct point representing (relative) position in world coordinates for a given x,y position
                        # ... in pixel space
                        point = np.array([self.normalized_vx(vx), self.normalized_vy(vy), self.normalized_vz(vz)])
                        xyz[x,y] = point

                    else:
                        # fill any remaining gaps
                        pixel_map[x, y] = materials[material][0], materials[material][1], \
                                          materials[material][2]
                        nearest_pixel_x, nearest_pixel_y = self.nearest_neighbour(np.array([[x, y]]))
                        vx, vy, vz = self.pixel2vertex[nearest_pixel_x, nearest_pixel_y]
                        point = np.array([self.normalized_vx(vx), self.normalized_vy(vy), self.normalized_vz(vz)])
                        xyz[x,y] = point


        im.convert("RGB")

        im.save("temp.png")
        im = Image.open("temp.png").convert("RGB")
        im = np.asarray(im)
        temp[:xyz.shape[0], :xyz.shape[1], xyz.shape[2]:] = xyz
        temp[:im.shape[0], :im.shape[1], :3] = im


        # uncomment to see both images inputted into the network

        # chiselled_img = temp[:,:,:3]/256
        # chiselled_img2 = temp[:, :, 3:] / 256
        # im = Image.fromarray(np.uint8(chiselled_img * 255))
        #im2 = Image.fromarray(np.uint8(chiselled_img2 * 255))
        # im.save("chiselled.png")
        # im.save("chiselled2.png")
        # print(chiselled_img)
        # print(chiselled_img.shape)

        tf.imwrite(dest_img, temp, (256,256,6))
        return True

    def nearest_neighbour(self, p):
        nearest_dist, nearest_ind = self.tree.query(p, k=1)
        return self.points[nearest_ind[:,0][0]]

    @staticmethod
    def scale_img(basewidth, img):
        wpercent = (basewidth / float(img.size[0]))
        hsize = int((float(img.size[1]) * float(wpercent)))
        im = img.resize((basewidth, hsize), Image.ADAPTIVE)
        return im

    @staticmethod
    def scale_img_dest(base_width, src, dest, mode):
        im = Image.open(src).convert(mode)
        im = MaterialPainter.scale_img(BLANK_IMG_WIDTH, im)
        im.save(dest)

import time

if __name__ == "__main__":
    start = time.time()
    mat_painter = MaterialPainter("obj_data.csv")
    # mat_painter.read_obj_data()
    # mat_painter.make_mask("texture.png", "test.tiff" , "Wood")

    im = MaterialPainter.draw_edges("texture.png", "horizontal", 10)
    im.save("xnp.png")

    print(time.time() - start)