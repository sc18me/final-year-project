from flask_wtf import Form
from wtforms import TextAreaField, IntegerField, SelectField
from wtforms.validators import DataRequired


class SettingsForm(Form):

    uv_map = SelectField('UV Map', choices=[("Default UV", "Default UV"), ("Smart UV", "Smart UV")])

    material = SelectField('Material', choices=[('Wood', 'Wood'),
                                                ('Rough Cloth', 'Rough Cloth'), ('Smooth Leather', 'Smooth Leather'),
                                                ('Metal', 'Metal'), ("Composite Board", "Composite Board")])

    sr_model = SelectField('Model', choices=[('RDN','RDN'), ('RRDN', 'RRDN')])

