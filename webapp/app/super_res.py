import numpy as np
from PIL import Image
from ISR.models import RDN, RRDN
import sys

def sr_inference(lr_fp, hr_fp, model):

    img = Image.open(lr_fp).convert("RGB")
    lr_img = np.array(img)

    if model == "RDN":
        m = RRDN(weights='gans')
    elif model == "RRDN":
        m = RDN(weights='noise-cancel')
    else:
        raise ValueError(f"{model} is not a valid ISR model. Choose from either RDN or RRDN")

    sr_img = m.predict(lr_img)
    img = Image.fromarray(sr_img)
    img.save(hr_fp)
    return


sr_inference(sys.argv[1], "app/out/val/images/sr.png", "RDN")