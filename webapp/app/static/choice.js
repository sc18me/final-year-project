

var slideIndex = 1;
positions = []
// connect to server

const canvas = document.querySelector("#canvas");
const canvas2 = document.querySelector("#canvas2");

const context = canvas.getContext("2d");
const context2 = canvas2.getContext("2d");

let img = document.getElementById("outline");
let img2 = document.getElementById("outline2");
let arrow = document.getElementById("arrow");

canvas.height = 256;
canvas.width = 256;

canvas2.height = 256;
canvas2.width = 256;

patch_length = 44;

context.drawImage(img, 10, 10);
context2.drawImage(img2, 10, 10);

var socket = io.connect("http://127.0.0.1:5000");


    socket.on('ready', function(msg) {

        $("#loading").hide();
        $("#result_btn").show();
        $("#result_btn").attr("href", msg);
        $("#result_text").attr("href", msg);

        console.log("received!");

});


$('#settings').on('click', function () {

    $("#loading").show();

    let mat = $("#mats :selected").val();

    let reference_texture = $("#ref_tex :selected").val();
    if (typeof reference_texture == "undefined")
        reference_texture = "False"
    // obtain the currently selected UV map on the carousel
    let active_uv;
    if (slideIndex === 1)
        active_uv = "default";
    else
        active_uv = "smart";

    let entry = {
        uv_map : active_uv,
        material : mat,
        points : positions,
        reference_texture : reference_texture
    }

    // trigger the event 'data_is_ready' so the callback function in the server can receive the msg
    socket.emit("data_is_ready", JSON.stringify(entry))
    console.log("sent!")
})

showSlides(slideIndex);

// Next/previous controls

function plusSlides(n) {
  showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
  showSlides(slideIndex = n);

}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
  console.log(slideIndex);
  positions = [];
  clear_canvases();

}

function clear_canvases() {
    context.clearRect(0, 0, canvas.width, canvas.height);
    context2.clearRect(0, 0, canvas2.width, canvas2.height);
    canvas.height = 256;
    canvas.width = 256;

    canvas2.height = 256;
    canvas2.width = 256;

    context.drawImage(img, 10, 10);
    context2.drawImage(img2, 10, 10);
    // reload canvas data

}

// canvas //

window.addEventListener("load", () => {

    function getMousePos(canvas, e) {
        var rect

        if (slideIndex === 1)
        {
            rect = canvas.getBoundingClientRect();
        }

        else if (slideIndex === 2)
        {
            rect = canvas2.getBoundingClientRect();
        }

        return {
          x: e.clientX - rect.left,
          y: e.clientY - rect.top
        };
}
    function mouse_pressdown(e) {
        pos = getMousePos(canvas, e);

        context.drawImage(arrow, pos.x-patch_length/2, pos.y-patch_length/2);
        context2.drawImage(arrow, pos.x-patch_length/2, pos.y-patch_length/2);

        positions.push(pos)
        socket.on("connect", function() {
        socket.send(slideIndex.toString())
        });
    }


    canvas.addEventListener("click", mouse_pressdown);
    canvas2.addEventListener("click", mouse_pressdown);

});
