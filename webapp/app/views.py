import shutil

from flask import render_template, flash, request, redirect, url_for, jsonify, make_response, session
from flask_socketio import SocketIO, send
from app import app, describe_uv
from app.describe_uv import MaterialPainter
from .forms import SettingsForm
from werkzeug.utils import secure_filename
import subprocess
import os
from shutil import copyfile
import logging
import app.concat as concat
import app.image_operations as image_operations
import json
import time

socketio = SocketIO(app)
socketio.run(app)
from PIL import Image

TEXTURE_UNWRAP_LOC = "app/unwrap_model.py"
BLENDER_LOC = "blender-2.92.0-linux64/blender"

UPLOAD_LOC = "app/uploads"
UPLOAD_MODELS_LOC = "app/uploads/models"
UPLOAD_TEXTURES_LOC = "app/uploads/textures"
STATIC_LOC = "app/static"
OBJ_DATA_LOC = "app/out/obj_data/object_data.csv"
ALLOWED_EXTENSIONS_MODEL = {"obj"}
ALLOWED_EXTENSIONS_IMAGE = {"png", "jpg", "jpeg"}

app.config['UPLOAD_MODEL_LOC'] = 'app/uploads/models/'
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0

logging.basicConfig(filename="activities.log", filemode="w", format='%(name)s - %(levelname)s - %(message)s')


def allowed_file_model(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS_MODEL


def allowed_file_image(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS_IMAGE


def setup_dirs():
    # setup upload dirs
    for directory in [UPLOAD_LOC, UPLOAD_TEXTURES_LOC, UPLOAD_MODELS_LOC,
                       "app/out/obj_data", "app/temp", "app/out/val", "app/out/val/val", "app/input/val/val/"]:
        if not os.path.exists(directory):
            os.makedirs(directory)

    if len(UPLOAD_TEXTURES_LOC) > 0:
        shutil.rmtree(UPLOAD_TEXTURES_LOC)

    os.makedirs(UPLOAD_TEXTURES_LOC)

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == "POST":
        if request.files:
            setup_dirs()

            file = request.files['file']
            for file in request.files.getlist("file"):
                if allowed_file_model(file.filename):
                    obj_file = file
                    file.save("app/uploads/models/1.obj")
                    print(file.filename + " stored as obj")

                else:
                    if allowed_file_image(file.filename):
                        image_file = file
                        image_file.save("app/uploads/textures/ref_texture.png")
                        print(file.filename + " stored as a reference image")

            start = time.time()
            subprocess.run(
                f"./app/obj_parser {UPLOAD_MODELS_LOC}{os.path.sep}1.obj app/out/obj_data/object_data.csv", shell=True
            )
            get_blank_texture()
            get_outlines()
            print(f"loading time: {time.time() - start}")
            return redirect('/generator_settings')

    return render_template("index.html", title="Home", user="Micah")


@app.route("/generator_settings", methods=['GET', 'POST'])
def generator_settings():
    form = SettingsForm()
    css_file = "choice.css"
    reference_texture = False
    if len(os.listdir(UPLOAD_TEXTURES_LOC)) > 0:
        reference_texture = True

    return render_template("generator_settings.html", css_file=css_file, form=form, reference_texture=reference_texture)


@socketio.on("data_is_ready")
def handle_message(msg):
    print("received data from client!")

    # convert string dictionary to dictionary
    msg = json.loads(msg)
    print(msg)

    uv_map = msg['uv_map']
    material = msg['material']
    points = msg['points']
    use_ref = msg['reference_texture']
    use_ref = str(use_ref) == "True"
    print(use_ref)

    points = [(point["x"], point["y"]) for point in points]

    start = time.time()
    if len(points) == 0 and use_ref is False:
        generate_texture(material, uv_map, None, None, use_ref)

    else:
        generate_texture(material, uv_map, points, "horizontal", use_ref)

    print(f"Texture generation time: {time.time() - start}")
    # notify client processing has finished
    socketio.emit("ready", f"http://127.0.0.1:5000/texture_choice/0/{uv_map}")


@app.route("/texture_choice/<choice>/<uv_mode>")
def texture_choice(choice, uv_mode):
    if choice != "0":

        sr_in = f"app/out/val/images/{choice}"
        image_operations.fill_bg(sr_in, sr_in)
        start = time.time()
        subprocess.run(f"python3 app/super_res.py {sr_in}", shell=True, stdout=subprocess.PIPE)
        subprocess.Popen(f"./{BLENDER_LOC} --python app{os.path.sep}auto_texture.py -- uv_mode {uv_mode}", shell=True)
        print(f"Upscaling time {time.time() - start}")

        return redirect(url_for("preview_texture"))

    return render_template("choice.html", uv_mode=uv_mode)


def get_outlines():
    mat_painter = MaterialPainter(None)
    outline_im1 = mat_painter.draw_outline(f"{STATIC_LOC}/in.png")
    outline_im2 = mat_painter.draw_outline(f"{STATIC_LOC}/in2.png")
    Image.fromarray(outline_im1).save(f"{STATIC_LOC}/outline1.png")
    Image.fromarray(outline_im2).save(f"{STATIC_LOC}/outline2.png")


def get_blank_texture():
    models = os.listdir(UPLOAD_MODELS_LOC)
    model_loc = f"{UPLOAD_MODELS_LOC}{os.path.sep}{models[0]}"
    output_loc = f"{STATIC_LOC}{os.path.sep}in.png"
    output_loc2 = f"{STATIC_LOC}{os.path.sep}in2.png"

    cmd = f"./{BLENDER_LOC} --python {TEXTURE_UNWRAP_LOC} -- model_location {model_loc} " \
          f"default_unwrap_out {output_loc} smart_project_out {output_loc2}"

    logging.info(cmd)
    blender = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE)


@app.route("/preview_model/")
def preview_texture():
    return render_template("view_model.html")


def generate_texture(material, uv_map, orientation_positions, orientation, use_reference_tex):

    if uv_map == "default":
        uv_in = f"{STATIC_LOC}{os.path.sep}in.png"
    else:
        uv_in = f"{STATIC_LOC}{os.path.sep}in2.png"


    # generate heightmap mask for a given object and desired material
    material_painter = MaterialPainter(OBJ_DATA_LOC)
    material_painter.read_obj_data()

    if orientation == None:
        material_painter.make_mask6(uv_in, "app/temp/features.tiff", material)
    else:
        material_painter.make_mask7(uv_in, "app/temp/features.tiff",
                                    material, orientation_positions, orientation, use_reference_tex)

    # use same format to create test images in order to help format files for gan inference
    if use_reference_tex:
        ref_texture_loc = os.listdir(UPLOAD_TEXTURES_LOC)[0]
        material_painter.convert_to_tiff_n(UPLOAD_TEXTURES_LOC+f"/{ref_texture_loc}", "app/temp/ref.tiff", 7)
        concat.concatenate_images("app/temp/features.tiff", "app/temp/ref.tiff", "app/input/val/val/input_image.tiff")
    else:
        concat.concatenate_images("app/temp/features.tiff", "app/temp/features.tiff", "app/input/val/val/input_image.tiff")

    # need to spawn processes to run scripts otherwise the memory will not deallocate after inference - workaround
    if orientation is None:
        print("no orientation")
        subprocess.run("./app/run_inference_no_orientation.sh", shell=True)

    else:
        subprocess.run("./app/run_inference.sh", shell=True)

    # copy choices to static
    copyfile("app/out/val/images/input_000_random_sample01.png", f"{STATIC_LOC}{os.path.sep}input_000_random_sample01.png")
    copyfile("app/out/val/images/input_000_random_sample02.png", f"{STATIC_LOC}{os.path.sep}input_000_random_sample02.png")
    copyfile("app/out/val/images/input_000_random_sample03.png", f"{STATIC_LOC}{os.path.sep}input_000_random_sample03.png")
    copyfile("app/out/val/images/input_000_random_sample04.png", f"{STATIC_LOC}{os.path.sep}input_000_random_sample04.png")



