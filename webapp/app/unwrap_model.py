import bpy
import os
import json
import shutil
import sys
import argparse


def unwrap_model(in_loc, out_loc, out_loc2):
    def blender_rm_all():
        bpy.data.batch_remove(bpy.data.objects)
        bpy.data.batch_remove(bpy.data.cameras)
        bpy.data.batch_remove(bpy.data.curves)
        bpy.data.batch_remove(bpy.data.images)
        bpy.data.batch_remove(bpy.data.textures)
        bpy.data.batch_remove(bpy.data.meshes)

    if len(bpy.data.objects) > 0:
        blender_rm_all()

    bpy.ops.import_scene.obj(filepath=in_loc)

    # set object as context
    bpy.context.view_layer.objects.active = bpy.data.objects[0]
    bpy.ops.object.mode_set(mode="EDIT")

    # export with default uv unwrapping - better for some models
    try:
        bpy.ops.uv.export_layout(filepath=out_loc,
        export_all=False, modified=False, mode='PNG', size=(256, 256), opacity=0.25,
        check_existing=True)

    except Exception as e:
        print("error: " + str(e))

    # export with smart uv project
    bpy.ops.uv.smart_project()
    bpy.ops.uv.export_layout(filepath=out_loc2,
                             export_all=False, modified=False, mode='PNG', size=(256, 256), opacity=0.25,
                             check_existing=True)

    bpy.ops.object.mode_set(mode="OBJECT")
    blender_rm_all()
    if len(bpy.data.objects) != 0:
        raise ValueError("There should be 0 objects in the scene")


parser = argparse.ArgumentParser()
parser.add_argument("--model_location", type=str)
parser.add_argument("--default_unwrap_out", type=str)
parser.add_argument("--smart_project_out", type=str)
args = parser.parse_known_args(sys.argv[sys.argv.index("--") + 1:])

model_location = args[1][1]
output_location = args[1][3]
output_location2 = args[1][5]

unwrap_model(model_location, output_location, output_location2)

bpy.ops.wm.quit_blender()
