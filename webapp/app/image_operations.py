from PIL import Image

def fill_bg(src_img, dest_img):
    """ fills background of an image/texture with the average pixel value of the texture"""

    im = Image.open(src_img).convert("RGB")
    pixel_map = im.load()

    # first pass - calculate average foreground pixel
    r = 0
    g = 0
    b = 0
    w, h = im.size
    count = 0
    for y in range(h):
        for x in range(w):

            # threshold for foreground
            if im.getpixel((x, y))[0] > 0 or im.getpixel((x, y))[0] > 0 or im.getpixel((x, y))[0] > 0:
                r += im.getpixel((x, y))[0]
                g += im.getpixel((x, y))[1]
                b += im.getpixel((x, y))[2]
                count += 1

    rgb_avr = int(r/count), int(r/count), int(r/count)

    # second pass - apply average rgb value
    for y in range(h):
        for x in range(w):

            if im.getpixel((x,y))[0] == 0 and im.getpixel((x,y))[0] == 0 and im.getpixel((x,y))[0] == 0:
                pixel_map[x,y] = rgb_avr[0], rgb_avr[1], rgb_avr[2]

    im.save(dest_img)
