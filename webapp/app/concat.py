import numpy as np
from PIL import Image
import cv2
import tifffile as tf

# def concatenate_images(img_A, img_B, dest):
#     """ Concatenates tiff images from domain A and B to form training sample image A->B of (2xwidth)xheight
#     """
#
#     rows = 256
#     cols = 256
#
#     img_B_padded = np.zeros((rows, cols, 6))
#
#     img_A = tf.imread(img_A)
#
#
#     img_B = MaterialPainter.scale_img(rows, img_B)
#     img_B = np.array(img_B)
#     img_B_padded[:rows,:cols,:3] = img_B
#
#     img_AB = np.concatenate([img_A, img_B], 1)
#     tf.imwrite(dest, img_AB, img_AB.shape)
#
#     rows_AB = 256
#     cols_AB = 512
#     A = img_AB[:rows_AB, :int(cols_AB/2), :6]
#     B = img_AB[:rows_AB, int(cols_AB/2):, :6]
#
#     # print(np.array_equal(A, img_A))
#     # print(np.array_equal(B, img_B_padded))


def concatenate_images(img_A, img_B, dest):
    """ Concatenates tiff images from domain A and B to form training sample image of shape (256, 512, 6) """
    img_A = tf.imread(img_A)
    img_B = tf.imread(img_B)

    img_AB = np.concatenate([img_A, img_B], 1)

    tf.imwrite(dest, img_AB)


