import bpy
import os
import argparse
import sys

def blender_rm_all():
    bpy.data.batch_remove(bpy.data.objects)
    bpy.data.batch_remove(bpy.data.cameras)
    bpy.data.batch_remove(bpy.data.curves)
    bpy.data.batch_remove(bpy.data.images)
    bpy.data.batch_remove(bpy.data.textures)
    bpy.data.batch_remove(bpy.data.meshes)
    bpy.data.batch_remove(bpy.data.images)


def bind_texture(model_fp, texture_fp, dest_fp, uv_mode):
    bpy.data.batch_remove(bpy.data.objects)
    bpy.ops.import_scene.obj(filepath=model_fp)

    if uv_mode == "smart":
        bpy.context.view_layer.objects.active = bpy.data.objects[0]
        bpy.ops.uv.smart_project()

    for obj in bpy.data.objects:
        bpy.context.view_layer.objects.active = obj
        bpy.ops.object.mode_set(mode="OBJECT")

    for i in range(100):
        bpy.ops.object.material_slot_remove()

    # apply texture to material
    mat = bpy.data.materials.new(name="new_mat")
    mat.use_nodes = True
    bsdf = mat.node_tree.nodes["Principled BSDF"]
    texImage = mat.node_tree.nodes.new('ShaderNodeTexImage')
    texImage.image = bpy.data.images.load(texture_fp)
    mat.node_tree.links.new(bsdf.inputs['Base Color'], texImage.outputs['Color'])

    # clear any materials from object

    bpy.ops.object.select_all(action='SELECT')

    # need to activate each obj and apply textures separately
    for obj in bpy.data.objects:
        bpy.context.view_layer.objects.active = obj
        bpy.ops.object.mode_set(mode="EDIT")
        if obj.data.materials:
            obj.data.materials[0] = mat
        else:
            obj.data.materials.append(mat)

    bpy.ops.export_scene.obj(filepath=dest_fp)

blender_rm_all()

parser = argparse.ArgumentParser()
parser.add_argument("--uv_mode", type=str)
args = parser.parse_known_args(sys.argv[sys.argv.index("--") + 1:])
uv_mode = args[1][1]

bind_texture("app/uploads/models/1.obj","app/out/val/images/sr.png", "final.obj", uv_mode)