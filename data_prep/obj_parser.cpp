#define TINYOBJLOADER_IMPLEMENTATION
#include "tiny_obj_loader.h"
#include <iostream>
#include <stdlib.h>
#include <map>
#include <time.h>

/**
might be a good idea to generate all possible coordinates that fall within the space of each face.
parallelise this if possible
 */
std::ofstream test_file = std::ofstream("test_file.csv");

typedef struct Face {
    tinyobj::real_t vx[3];
    tinyobj::real_t vy[3];
    tinyobj::real_t vz[3];

    tinyobj::real_t tx[3];
    tinyobj::real_t ty[3];

}Face;

std::tuple<int, int> uv2pixel_space(float s, float t, int w, int h) {
    return std::make_tuple((s*w)-0.5, ((1-t)*h) -0.5);
}

void interpolate_points(Face& face, std::ofstream& out_file, std::map<std::tuple<int, int>, int>& pixel_map) {
    /**
     *  perform barycentric interpolation to find
     *  1. vertex positions
     *  2. texture coordinates
     *
     */

    tinyobj::real_t px;
    tinyobj::real_t py;
    tinyobj::real_t pz;

    tinyobj::real_t tx;
    tinyobj::real_t ty;

   for(int i = 0; i < 3; ++i)
   {
        test_file << face.vx[i] << "," << face.vy[i] << "," << face.vz[i] << "," << face.tx[i] << "," << face.ty[i] << ",";
   }
   test_file << "\n";

    for (float t=0; t<=1.0; t += 0.04)
        for(float s=0; s<=1.0; s += 0.04)
        {
            px = (1-s)*(1-t)*face.vx[0] + (1-s)*t*face.vx[1] + s*face.vx[2];
            py = (1-s)*(1-t)*face.vy[0] + (1-s)*t*face.vy[1] + s*face.vy[2];
            pz = (1-s)*(1-t)*face.vz[0] + (1-s)*t*face.vz[1] + s*face.vz[2];

            tx = (1-s)*(1-t)*face.tx[0] + (1-s)*t*face.tx[1] + s*face.tx[2];
            ty = (1-s)*(1-t)*face.ty[0] + (1-s)*t*face.ty[1] + s*face.ty[2];


            auto result = uv2pixel_space(tx, ty, 256, 256);
            if (!pixel_map.count(result)) {
                pixel_map[result] = 1;
                out_file << px << "," <<  py << "," << pz << "," << std::get<0>(result) << "," << std::get<1>(result)<< std::endl;
            }
        }

    std::cout << px << " " << py << " " << pz << std::endl;
}

int main(int argc, char** argv) {

    if (argc != 3) {
        std::cerr << "Usage: <model_name.obj> <output_fp>" << std::endl;
        exit(0);
    }
    std::string fp = argv[1];

    clock_t tStart = clock();
    std::string inputfile = argv[1];
    tinyobj::ObjReaderConfig reader_config;
    reader_config.mtl_search_path = "./"; // Path to material files

    tinyobj::ObjReader reader;

    std::ofstream file = std::ofstream(argv[2]);
    file << "vx" << "," <<  "vy" << "," << "vz" << "," << "pixel_x" << "," << "pixel_y" << std::endl;


    if (!reader.ParseFromFile(inputfile, reader_config)) {
        if (!reader.Error().empty()) {
            std::cerr << "TinyObjReader: " << reader.Error();
        }

        exit(1);
    }

    if (!reader.Warning().empty()) {
        std::cout << "TinyObjReader: " << reader.Warning();
    }

    Face** face_store;

    int n_faces = 0;

    int count = 0;
    int vertex_count = 0;

    std::map<std::tuple<int, int>, int> pixel_map;

    auto& attrib = reader.GetAttrib();
    auto& shapes = reader.GetShapes();

    // Loop over shapes
    for (size_t s = 0; s < shapes.size(); s++) {
        // Loop over faces(polygon)
        size_t index_offset = 0;

        for (size_t f = 0; f < shapes[s].mesh.num_face_vertices.size(); f++) {
            n_faces += 1;
            Face face;

            int fv = shapes[s].mesh.num_face_vertices[f];

            // Loop over vertices in the face.
            for (size_t v = 0; v < fv; v++) {
                // access to vertex
                tinyobj::index_t idx = shapes[s].mesh.indices[index_offset + v];
                tinyobj::real_t vx = attrib.vertices[3*idx.vertex_index+0];
                tinyobj::real_t vy = attrib.vertices[3*idx.vertex_index+1];
                tinyobj::real_t vz = attrib.vertices[3*idx.vertex_index+2];
                tinyobj::real_t nx = attrib.normals[3*idx.normal_index+0];
                tinyobj::real_t ny = attrib.normals[3*idx.normal_index+1];
                tinyobj::real_t nz = attrib.normals[3*idx.normal_index+2];
                tinyobj::real_t tx = attrib.texcoords[2*idx.texcoord_index+0];
                tinyobj::real_t ty = attrib.texcoords[2*idx.texcoord_index+1];

                face.vx[v] = vx;
                face.vy[v] = vy;
                face.vz[v] = vz;

                face.tx[v] = tx;
                face.ty[v] = ty;

                vertex_count++;

            }

            interpolate_points(face, file, pixel_map);

            index_offset += fv;
        }
    }
    /* Do your stuff here */
    printf("Time taken: %.2fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);
    std::cout << "Vertex count: " << vertex_count << std::endl;
    return 0;
}
