import os

import tifffile as tf
from PIL import Image
import numpy as np
import cv2
from describe_uv import MaterialPainter
from scipy.interpolate import barycentric_interpolate


def open_image(src_img):
    imgA = tf.imread(src_img)

    chiselled1 = imgA[:, :, :3]
    chiselled2 = imgA[:, :, 3:6]
    chiselled3 = imgA[:, :, 6]

    print(chiselled1.shape)
    Image.fromarray(np.uint8(chiselled1)).save("chiselled1.png")
    Image.fromarray(np.uint8(chiselled2)).save("chiselled2.png")
    Image.fromarray(np.uint8(chiselled3)).save("chiselled3.png")

# open_image("/home/micah/year3/dissertation/datasets/3D-FUTURE-model/blank-uv-maps/0b7c1001-ab11-4161-8c19-b8a9e7067988.tiff")
# open_image("/home/micah/year3/dissertation/datasets/3D-FUTURE-model/blank-uv-maps/10.tiff")


# talk about the results of the speedup briefly.

def test_barycentric_interpolation(data_fp):
    """ takes SSD between my implementation barycentric interpolation and scipys """

    # calculate interpolation with scipy
    with open(data_fp, "r") as f:
        with open("obj_data_test.csv", "w") as fw:
            line = f.readline()
            fw.write("vx,vy,vz,pixel_x,pixel_y\n")

            while line != "":
                polygon = line.split(",")[:-1]
                x = [polygon[0], polygon[5], polygon[10]]
                y = [polygon[1], polygon[6], polygon[11]]
                z = [polygon[2], polygon[7], polygon[12]]
                tx = [polygon[3], polygon[8], polygon[13]]
                ty = [polygon[4], polygon[9], polygon[14]]


                for dim in [x,y,z,tx,ty]:
                    for i, val in enumerate(dim):
                        dim[i] = float(val)

                n_points = 2
                x2 = np.linspace(min(x), max(x), n_points)
                y2 = np.linspace(min(y), max(y), n_points)
                tx2 = np.linspace(min(tx), max(tx), n_points)
                ty2 = np.linspace(min(ty), max(ty), n_points)

                px = barycentric_interpolate(y, x, y2)
                py = barycentric_interpolate(x, y, x2)
                pz = barycentric_interpolate(x, z, x2)

                next_tx = barycentric_interpolate(tx, ty, tx2)
                next_ty = barycentric_interpolate(ty, tx, ty2)

                for i in range(len(px)):
                    fw.write(f"{px[i]},{py[i]},{pz[i]},{int((next_tx[i]*255)-0.5)},{int(((1-next_ty[i])*255) -0.5)}\n")


                # print(px, py, pz)
                line = f.readline()

    mat_painter_test = MaterialPainter("obj_data_test.csv")
    mat_painter = MaterialPainter("obj_data.csv")

    mat_painter.read_obj_data()
    mat_painter_test.read_obj_data()

    mat_painter_test.make_mask("texture.png", "real.tiff", "Wood")
    mat_painter.make_mask("texture.png", "mine.tiff", "Wood")

    # load the two images into memory
    real = tf.imread("real.tiff")
    mine = tf.imread("mine.tiff")

    cv2.imwrite("real.png", real[:,:,3:6])
    cv2.imwrite("mine.png", mine[:,:,3:6])
    real = real.flatten()
    mine = mine.flatten()
    print(real.shape)
    for i in real.flatten():
        ()

test_barycentric_interpolation("test_file.csv")

real = tf.imread("real.tiff")
mine = tf.imread("mine.tiff")

cv2.imwrite("real.png", real[:, :, 3:6])
cv2.imwrite("mine.png", mine[:, :, 3:6])
real = real.flatten()
mine = mine.flatten()
dif = real.ravel() - mine.ravel()
ssd = np.dot(dif,dif)
print(ssd)
