import numpy as np
import tifffile as tf

def concatenate_images(img_A, img_B, dest):
    """ Concatenates tiff images from domain A and B to form training sample image of shape (256, 512, n_channels) """
    img_A = tf.imread(img_A)
    img_B = tf.imread(img_B)

    img_AB = np.concatenate([img_A, img_B], 1)

    tf.imwrite(dest, img_AB)

