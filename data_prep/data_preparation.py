import os
import json
from data_understanding import categories
import argparse
import shutil
import random
import sys
import time
import augmentation


DATA_ROOT = "/home/micah/year3/dissertation/datasets/3D-FUTURE-model"
METADATA = os.path.join(DATA_ROOT, "model_info.json")
PARENT_DATASET = os.path.join(DATA_ROOT, "3D-FUTURE-model")
CHILD_DATASET = os.path.join(DATA_ROOT, "actual")
IMAGE_PATH = os.path.join(CHILD_DATASET, "image")
N_BLANKS = 200
BLANK_DATASET = os.path.join(DATA_ROOT, "blank-uv-maps")

# ANNOTATION_FILE = os.path.join(CHILD_DATASET, "anno", "list_attr_celeba.txt")

CATEGORIES = [categories._ATTR_STYLE, categories._ATTR_MATERIAL, categories._ATTR_THEME]


cmd_style_attr = {"modern": "Modern", "chi": "Chinoiserie", "euro":"European", "kids":"Kids", "japanese":"Japanese",
                  "vintage": "Vintage/Retro", "industrial": "Industrial", "med": "Mediterranean"
                  ,"min": "Minimalist", "chinese": "New Chinese", "qing": "Ming Qing", "asia": "Southeast Asia",
                  "korean": "Korean", "neo": "Neoclassical", "american": "American Country", "nordic": "Nordic",
                  "euroclassic": "European Classic", "others": "Others", "lightluxury": "Light Luxury"}

cmd_material_attr = {'composition': 'Composition','cloth': 'Cloth', 'leather': 'Leather', 'glass': 'Glass', 'metal': 'Metal',
    'solidwood': 'Solid Wood', 'stone': 'Stone', "plywood": 'Plywood', "others": 'Others', "suede": 'Suede',
    "bamboo": 'Bamboo Rattan', "roughcloth": 'Rough Cloth', "wood": 'Wood', 'board': 'Composite Board', 'marble': 'Marble',
    'sleather': 'Smooth Leather'}


COMBINED_ATTR = categories._ATTR_STYLE + categories._ATTR_MATERIAL + categories._ATTR_THEME

def process_args():
    if not args.materials is None:
        for attr in args.materials:
            if not attr in cmd_material_attr:
                print(f"'{attr}' is not a valid material", file=sys.stderr)
                sys.exit(0)
            material[cmd_material_attr[attr]] = None

    if not args.style is None:
        for attr in args.style:
            if not attr in cmd_style_attr:
                print(f"'{attr}' is not a valid style", file=sys.stderr)
                sys.exit(0)
            style[cmd_style_attr[attr]] = None




class UV_dataset:
    def __init__(self, style_attr, material_attr, include_notexture, p_blank):

        self.p_blank = p_blank
        self.total_count = 0
        self.textured_idx = 0
        self.untextured_idx = 0
        self.attribute_count = 0
        self.max_untextured = len(os.listdir(BLANK_DATASET))
        # self.transformation = [None, augmentation.rotate, augmentation.rotate, augmentation.rotate, augmentation.mirror]
        self.transformation = [None]
        self.writer = None
        self.data = None
        self.category_dict = {'Cabinet/Shelf/Desk': None, 'Table': None, 'Chair': None }
        self.style_attr = {"Modern", "Japanese", "Industrial", "Light Luxury", "Korean", "Minimalist"}
        self.material_attr = material_attr
        self.theme_attr = {"None"}
        self.category_table = None
        self.relevant_attr = [attr['category'] for attr in COMBINED_ATTR if attr['category'] in self.material_attr or
                              attr['category'] in self.style_attr or attr['category'] in self.theme_attr]
        self.include_notexture = include_notexture

        if os.path.exists(CHILD_DATASET):
            shutil.rmtree(CHILD_DATASET)

        os.mkdir(CHILD_DATASET)
        os.mkdir(IMAGE_PATH)

        self.build_dataset()

    def prep_blank_data(self):
        """Takes a blank uv atlas and copies it into actual dataset"""

        # perform len(self.transformation)) no. writes to the file
        src = f"{BLANK_DATASET}{os.path.sep}{str(self.untextured_idx)}.png"
        angle = 90
        idx = 0
        for transform in self.transformation:
            dest = f"{IMAGE_PATH}{os.path.sep}{str(self.total_count+1+idx)}.png"
            self.writer.write(f"{str(self.total_count+1+idx)}.png ")

            for j in range(self.attribute_count - 1):
                self.writer.write(f"{str(-1)} ")

            if self.include_notexture:
                self.writer.write(f"{str(1)}\n")
            else:
                self.writer.write("\n")

            if transform == augmentation.rotate:
                transform(src, dest, angle)
                angle += 90

            if transform == augmentation.mirror:
                transform(src, dest)

            idx += 1
        # copies original file onto new
        self.cp_blank_data()
        # we made four copies of one file. Only increment for original files
        self.untextured_idx += 1

    def prep_textured_data(self):
        model = self.data[self.textured_idx]
        if model['material'] in self.material_attr and model['style'] in self.style_attr:

            if self.untextured_idx < self.max_untextured and random.randint(0, 10) < self.p_blank * 10:
                self.prep_blank_data()
                self.total_count += len(self.transformation)

            idx = 0
            angle = 90
            src = f"{PARENT_DATASET}{os.path.sep}{model['model_id']}{os.path.sep}texture.png"
            for transform in self.transformation:
                self.writer.write(f"{str(self.total_count + 1 + idx)}.png ")
                # write down attribute states for each model
                for attr in self.relevant_attr:
                    # apply mask to extract tags
                    if attr == model['material'] or attr == model['style'] or attr == model['theme']:
                        self.writer.write(f"{str(1)} ")
                    else:
                        self.writer.write(f"{str(-1)} ")

                # -1 for no texture attribute
                if self.include_notexture:
                    self.writer.write(f"{str(-1)}\n")
                else:
                    self.writer.write("\n")

                dest = f"{CHILD_DATASET}{os.path.sep}image{os.path.sep}{str(self.total_count+1+idx)}.png"
                if transform == augmentation.rotate:
                    transform(src, dest, angle)
                    angle += 90

                elif transform == augmentation.mirror:
                    transform(src, dest)

                idx += 1

            self.cp_textured_data(model['model_id'])
            self.total_count += len(self.transformation)

    def cp_blank_data(self):
        # self.total_count + 1 since we need to start from 1..n not 0..n
        print(f"Copying {BLANK_DATASET}/{str(self.untextured_idx)}.png - Pasting into {IMAGE_PATH}/{str(self.total_count+1)}.png")
        shutil.copy(os.path.join(BLANK_DATASET, str(self.untextured_idx) + ".png"), os.path.join(IMAGE_PATH, str(self.total_count+1) + ".png"))

    def cp_textured_data(self, model_id):
        print(f"Copying {PARENT_DATASET}/{model_id}/texture.png - Pasting into {CHILD_DATASET}/image/"
              f"{str(self.total_count+1)}.png")

        shutil.copy(os.path.join(PARENT_DATASET, model_id, "texture.png"),
                    os.path.join(IMAGE_PATH, str(self.total_count+1)+".png"))

    def build_dataset(self):
        """generates annotation.txt file s.t user specified dataset """
        anno_path = os.path.join(CHILD_DATASET, "anno")
        os.mkdir(anno_path)
        annotation_file = os.path.join(anno_path, "list_attr_celeba.txt")

        with open(METADATA) as json_file:
            self.data = json.load(json_file)
            with open(annotation_file, "w") as self.writer:
                self.category_table = {}
                # write attribute types
                # leave space for the instance total
                self.writer.write("\n")
                idx = 0
                for category in CATEGORIES:
                    for attribute_type in category:
                        if attribute_type['category'] in self.style_attr or attribute_type['category'] in self.material_attr or attribute_type['category'] in self.theme_attr:
                            self.writer.write(attribute_type['category'].replace(" ", "_") + " ")
                            self.category_table[attribute_type['category']] = idx
                            idx += 1

                if self.include_notexture:
                    self.writer.write("no_texture\n")

                else:
                    self.writer.write("\n")

                self.attribute_count = idx + 1
                while self.textured_idx < len(self.data):
                    self.prep_textured_data()
                    self.textured_idx += 1

                self.writer.close()

            # write no. instances to the file
            with open(annotation_file, "r+") as appender:
                content = appender.read()
                appender.seek(0, 0)
                appender.write(str(self.total_count) + content)
                appender.close()

            json_file.close()


parser = argparse.ArgumentParser()
parser.add_argument("--materials", type=str, help=print(f"Materials: {cmd_material_attr}\n"), nargs="+")
parser.add_argument("--style", type=str, help=print(f"Styles: {cmd_style_attr}\n"), nargs="+")
parser.add_argument("--themes", type=str, help=print(f"Themes: {cmd_style_attr}\n"), nargs="+")
style = {}
material = {"Composition", "Metal", "Solid Wood", "Wood", "Composite Board", "Marble"}
args = parser.parse_args()
process_args()
s = time.time()
uv_dataset = UV_dataset(style, material, True, 0.3)
print(time.time() - s)
