import os
import shutil
import json
import random
from PIL import Image, ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True


DATA_ROOT = "/home/micah/year3/dissertation/datasets/3D-FUTURE-model"
METADATA = os.path.join(DATA_ROOT, "model_info.json")
TEXTURED_DATASET = os.path.join(DATA_ROOT, "textured-uv-maps")
BLANK_DATASET = os.path.join(DATA_ROOT, "blank-uv-maps")

PARENT_DATASET = os.path.join(DATA_ROOT, "3D-FUTURE-model")
IMAGE_WIDTH = 250

materials = {"Composite Board": (102, 51, 0), "Wood": (204, 102, 0), "Smooth Leather": (204, 255, 255),
             "Rough Cloth": (255,255,153), "Metal": (255,0,0) }


def cp_textured_data(model_id, idx):
    print(f"Copying {PARENT_DATASET}/{model_id}/texture.png - Pasting into {TEXTURED_DATASET}"
          f"{str(idx)}.png")

    shutil.copy(f"{PARENT_DATASET}{os.path.sep}{model_id}{os.path.sep}texture.png", f"{TEXTURED_DATASET}{os.path.sep}{str(idx)}.png")


def prepare_data():
    with open(METADATA) as json_file:
        data = json.load(json_file)
        idx = 0
        for model in data:
            if model['material'] in materials:
                cp_textured_data(model['model_id'], idx)
                idx += 1

def setup_folders(fp_textured, fp_untextured):

    if os.path.exists(f"{fp_textured}{os.path.sep}train"):
        shutil.rmtree(f"{fp_textured}{os.path.sep}train")

    if os.path.exists(f"{fp_textured}{os.path.sep}test"):
        shutil.rmtree(f"{fp_textured}{os.path.sep}test")

    if os.path.exists(f"{fp_textured}{os.path.sep}val"):
        shutil.rmtree(f"{fp_textured}{os.path.sep}val")

    if os.path.exists(f"{fp_untextured}{os.path.sep}train"):
        shutil.rmtree(f"{fp_untextured}{os.path.sep}train")

    if os.path.exists(f"{fp_untextured}{os.path.sep}test"):
        shutil.rmtree(f"{fp_untextured}{os.path.sep}test")

    if os.path.exists(f"{fp_untextured}{os.path.sep}val"):
        shutil.rmtree(f"{fp_untextured}{os.path.sep}val")

    os.mkdir(f"{fp_textured}{os.path.sep}train")
    os.mkdir(f"{fp_textured}{os.path.sep}test")
    os.mkdir(f"{fp_textured}{os.path.sep}val")
    os.mkdir(f"{fp_untextured}{os.path.sep}train")
    os.mkdir(f"{fp_untextured}{os.path.sep}test")
    os.mkdir(f"{fp_untextured}{os.path.sep}val")



def split_dataset(fp_textured, fp_untextured):
    """splits dataset into the train test val folders
    """
    setup_folders(fp_textured, fp_untextured)

    ignore = {"train":1, "test":1, "val":1}
    samples = os.listdir(fp_textured)
    for sample in samples:
        if str(sample) in ignore:
            continue

        if random.randint(1, 10) < 7.5:
            shutil.move(f"{fp_textured}{os.path.sep}{sample}",f"{fp_textured}{os.path.sep}train{os.path.sep}{sample}")
            print(f"{fp_textured}{os.path.sep}{sample} -> {fp_textured}{os.path.sep}train{os.path.sep}{sample}")
            shutil.move(f"{fp_untextured}{os.path.sep}{sample}", f"{fp_untextured}{os.path.sep}train{os.path.sep}{sample}")
            scale_down_img(f"{fp_textured}{os.path.sep}train{os.path.sep}{sample}", IMAGE_WIDTH)
            scale_down_img(f"{fp_untextured}{os.path.sep}train{os.path.sep}{sample}", IMAGE_WIDTH)

        else:
            if random.randint(1, 10) > 7:
                shutil.move(f"{fp_textured}{os.path.sep}{sample}", f"{fp_textured}{os.path.sep}test{os.path.sep}{sample}")
                print(f"{fp_textured}{os.path.sep}{sample} -> {fp_textured}{os.path.sep}test{os.path.sep}{sample}")
                shutil.move(f"{fp_untextured}{os.path.sep}{sample}", f"{fp_untextured}{os.path.sep}test{os.path.sep}{sample}")
                scale_down_img(f"{fp_textured}{os.path.sep}test{os.path.sep}{sample}", IMAGE_WIDTH)
                scale_down_img(f"{fp_untextured}{os.path.sep}test{os.path.sep}{sample}", IMAGE_WIDTH)
            else:
                shutil.move(f"{fp_textured}{os.path.sep}{sample}", f"{fp_textured}{os.path.sep}val{os.path.sep}{sample}")
                print(f"{fp_textured}{os.path.sep}{sample} -> {fp_textured}{os.path.sep}val{os.path.sep}{sample}")
                shutil.move(f"{fp_untextured}{os.path.sep}{sample}", f"{fp_untextured}{os.path.sep}val{os.path.sep}{sample}")
                scale_down_img(f"{fp_textured}{os.path.sep}val{os.path.sep}{sample}", IMAGE_WIDTH)
                scale_down_img(f"{fp_untextured}{os.path.sep}val{os.path.sep}{sample}", IMAGE_WIDTH)


def scale_down_img(img_fp, base_width):
    "scales down image at a given file path to a base width keeping its aspect ratio"
    img = Image.open(img_fp)
    wpercent = (base_width / float(img.size[0]))
    hsize = int((float(img.size[1]) * float(wpercent)))
    img = img.resize((base_width, hsize), Image.ANTIALIAS)
    img.save(img_fp)


prepare_data()
split_dataset(TEXTURED_DATASET, BLANK_DATASET)