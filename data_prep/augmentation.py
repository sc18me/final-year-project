from PIL import Image, ImageOps, ImageFile
import os
import shutil
ImageFile.LOAD_TRUNCATED_IMAGES = True

rotate = lambda src, dest, angle: Image.open(src).rotate(angle).save(dest)
mirror = lambda src, dest: ImageOps.mirror(Image.open(src)).save(dest)

