import bpy
import os
import json
import shutil
import sys
import PIL
import subprocess

DATA_ROOT = "/home/micah/year3/dissertation/datasets/3D-FUTURE-model"
DATA_PREP = "/home/micah/year3/dissertation/src/project_src/data_prep/"

METADATA = os.path.join(DATA_ROOT, "model_info.json")
PARENT_DATASET = os.path.join(DATA_ROOT, "3D-FUTURE-model")
CHILD_DATASET = os.path.join(DATA_ROOT, "actual")
BLANK_DATASET = os.path.join(DATA_ROOT, "blank-uv-maps")
loc = "/home/micah/year3/dissertation/datasets/3D-FUTURE-model/3D-FUTURE-model"


def blender_rm_all():
    bpy.data.batch_remove(bpy.data.objects)
    bpy.data.batch_remove(bpy.data.cameras)
    bpy.data.batch_remove(bpy.data.curves)
    bpy.data.batch_remove(bpy.data.images)
    bpy.data.batch_remove(bpy.data.textures)
    bpy.data.batch_remove(bpy.data.meshes)


def build_blank_dataset(materials):
    idx = 0
    if os.path.exists(BLANK_DATASET):
        shutil.rmtree(BLANK_DATASET)

    os.mkdir(os.path.join(BLANK_DATASET))
    with open(os.path.join(DATA_ROOT, METADATA)) as json_file:
        data = json.load(json_file)

        if len(bpy.data.objects) > 0:
            blender_rm_all()

        for model in data:
            if model['material'] in materials:
                bpy.ops.import_scene.obj(filepath=os.path.join(loc, model["model_id"], "normalized_model.obj"))
                bpy.context.view_layer.objects.active = bpy.data.objects[0]
                bpy.ops.object.mode_set(mode="EDIT")
                fp = f"{BLANK_DATASET}{os.path.sep}{str(idx)}.png"
                bpy.ops.uv.export_layout(filepath=fp,
                                         export_all=False, modified=False, mode='PNG', size=(256, 256), opacity=0.25,
                                         check_existing=True)

                bpy.ops.object.mode_set(mode="OBJECT")
                blender_rm_all()
                idx += 1
                if len(bpy.data.objects) != 0:
                    raise ValueError("There should be 0 objects in the scene")


materials = {"Composite Board", "Wood", "Smooth Leather",
             "Rough Cloth", "Metal"}
build_blank_dataset(materials)