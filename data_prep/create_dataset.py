import os
import shutil
import json
from describe_uv import MaterialPainter
import subprocess
import numpy as np
import random
from PIL import Image, ImageFile
import tifffile as tf
import pickle
import concat
import cv2
import time
import concurrent.futures

ImageFile.LOAD_TRUNCATED_IMAGES = True

DATA_ROOT = "/home/micah/year3/dissertation/datasets/3D-FUTURE-model"
DATA_PREP = "/home/micah/year3/dissertation/src/project_src/data_prep/"

IMG_WIDTH = 256

METADATA = os.path.join(DATA_ROOT, "model_info.json")
PARENT_DATASET = os.path.join(DATA_ROOT, "3D-FUTURE-model")
CHILD_DATASET = os.path.join(DATA_ROOT, "actual")
BLANK_DATASET = os.path.join(DATA_ROOT, "blank-uv-maps")
TEXTURED_DATASET = os.path.join(DATA_ROOT, "textured-uv-maps2")


AB_LOC = f"{DATA_ROOT}{os.path.sep}AB"

models_loc = "/home/micah/year3/dissertation/datasets/3D-FUTURE-model/3D-FUTURE-model"
MODEL_CSV_DATA = "/home/micah/year3/dissertation/src/project_src/data_prep/object_data.csv"


class Dataset:

    def __init__(self, materials, A_pth, B_pth, AB_pth):
        self.A_pth = A_pth
        self.B_pth = B_pth
        self.AB_pth = AB_pth
        self.materials = materials
        self.omitted_data = {}

    def interpolation(self, model_loc, id):
        obj_loader = subprocess.run(f"./obj_parser {model_loc} obj_data{id}.csv", shell=True,
                                    stdout=subprocess.PIPE)

    def extract_features(self, model, id):
        self.material_painter = MaterialPainter(f"obj_data{id}.csv")

        instance_root = f"{models_loc}{os.path.sep}{model['model_id']}"
        model_loc = f"{instance_root}{os.path.sep}normalized_model.obj"
        model_texture_loc = f"{instance_root}{os.path.sep}texture.png"

        blank_texture_loc = f"{BLANK_DATASET}{os.path.sep}{model['model_id']}.tiff"
        textured_loc = f"{TEXTURED_DATASET}{os.path.sep}{model['model_id']}.tiff"

        print(f"Currently processing: {model['model_id']}")

        # get model data

        obj_loader = subprocess.run(f"./obj_parser {model_loc} obj_data{id}.csv", shell=True,
                                    stdout=subprocess.PIPE)

        # create mask using MaterialPainter obj
        # if either fail, log bad models
        if not self.material_painter.read_obj_data():
            self.omitted_data[model['model_id']] = 1
            return False

        if not self.material_painter.make_mask(model_texture_loc, blank_texture_loc, model['material']):
            self.omitted_data[model['model_id']] = 1
            return False

        # create target example
        MaterialPainter.convert_to_tiff6(model_texture_loc, textured_loc)
        return True


    def create_dataset_parallel(self):
        """ creates separate datasets A and B """
        start_time = time.time()

        if os.path.exists(BLANK_DATASET):
            shutil.rmtree(BLANK_DATASET)

        if os.path.exists(TEXTURED_DATASET):
            shutil.rmtree(TEXTURED_DATASET)

        os.mkdir(BLANK_DATASET)
        os.mkdir(TEXTURED_DATASET)

        with open(os.path.join(DATA_ROOT, METADATA)) as json_file:

            data = json.load(json_file)
            idx = 0
            batch_size = 10
            batch = []
            for model in data:
                if model['material'] in self.materials:
                    batch.append(model)
                    if len(batch) == batch_size:
                        with concurrent.futures.ThreadPoolExecutor() as executor:
                            for i, model in enumerate(batch):
                                executor.submit(self.extract_features, model, i)

                        batch.clear()
                        idx += batch_size

                if idx == 20:
                    print(f"Parallel time: {time.time() - start_time}")
                    exit(0)


    def create_blank_dataset(self, use_checkpoint):
        """ creates separate datasets A and B """
        self.material_painter = MaterialPainter("obj_data.csv")
        start_time = time.time()
        checkpoint = 0
        if not use_checkpoint:
            if os.path.exists(BLANK_DATASET):
                shutil.rmtree(BLANK_DATASET)

            if os.path.exists(TEXTURED_DATASET):
                shutil.rmtree(TEXTURED_DATASET)

            os.mkdir(BLANK_DATASET)
            os.mkdir(TEXTURED_DATASET)

        else:
            checkpoint = len(os.listdir(BLANK_DATASET)) - 2
            if os.path.getsize("omitted.pickle") > 0:
                omitted_fr = open("omitted.pickle", "rb")
                self.omitted_data = pickle.load(omitted_fr)

        omitted_fw = open("omitted.pickle", "wb")

        with open(os.path.join(DATA_ROOT, METADATA)) as json_file:

            data = json.load(json_file)
            idx = 0
            for model in data:
                if model['material'] in self.materials:
                    # save state of omitted for checkpoints

                    instance_root = f"{models_loc}{os.path.sep}{model['model_id']}"
                    model_loc = f"{instance_root}{os.path.sep}normalized_model.obj"
                    model_texture_loc = f"{instance_root}{os.path.sep}texture.png"

                    blank_texture_loc = f"{BLANK_DATASET}{os.path.sep}{str(idx)}.tiff"
                    textured_loc = f"{TEXTURED_DATASET}{os.path.sep}{str(idx)}.tiff"

                    print(f"Currently processing: {model['model_id']}")

                    if idx >= checkpoint:
                        # get model data
                        obj_loader = subprocess.run(f"./obj_parser {model_loc} obj_data.csv", shell=True,
                                                    stdout=subprocess.PIPE)

                        # create mask using MaterialPainter obj
                        # if either fail, log bad models
                        if not self.material_painter.read_obj_data():
                            self.omitted_data[model['model_id']] = 1
                            # keep idx on track with the NUMBER OF MODELS PROCESSED. this is so that we can track
                            # what models are in our train/test/val - we could use model name AS filename instead?
                            idx += 1
                            continue

                        if not self.material_painter.make_mask(model_texture_loc, blank_texture_loc, model['material']):
                            self.omitted_data[model['model_id']] = 1
                            idx += 1
                            continue

                        # create target example
                        MaterialPainter.convert_to_tiff6(model_texture_loc, textured_loc)

                    # TODO move created mask to dataset
                    print(f"Models processed: {idx+1}")
                    idx += 1

                    if idx == 20:
                        print("Serial time: " + str(time.time() - start_time))
                        exit(0)

            json_file.close()

    @staticmethod
    def setup_dataset_dir(root_dir):
        """ creates a directory containing train, test, val """

        # check for existing folders and remove if they exist
        if not os.path.exists(root_dir):
            os.mkdir(root_dir)

        for subset in (['train', 'test', 'val']):
            if os.path.exists(f"{root_dir}{os.path.sep}{subset}"):
                shutil.rmtree(f"{root_dir}{os.path.sep}{subset}")

            os.mkdir(f"{root_dir}{os.path.sep}{subset}")

    def setup_AB_dataset(self):
        Dataset.setup_dataset_dir(self.AB_pth)

    def create_AB_dataset(self):
        """ creates a dataset of alligned examples from domains A and B """

        self.setup_AB_dataset()
        n_examples = len(os.listdir(BLANK_DATASET))

        if len(os.listdir(self.A_pth)) != len(os.listdir(self.B_pth)):
            print("Error: datasets do not match in size")
            exit(0)

        for i, image in enumerate(os.listdir(self.A_pth)):
            if random.randint(1, 10) < 8:
                concat.concatenate_images(f"{self.A_pth}{os.path.sep}{image}",
                                          f"{self.B_pth}{os.path.sep}{image}",
                                          f"{self.AB_pth}{os.path.sep}train{os.path.sep}{image}")
            else:
                if random.randint(1, 10) > 7:
                    concat.concatenate_images(f"{self.A_pth}{os.path.sep}{image}",
                                              f"{self.B_pth}{os.path.sep}{image}",
                                              f"{self.AB_pth}{os.path.sep}test{os.path.sep}{image}")

                else:
                    concat.concatenate_images(f"{self.A_pth}{os.path.sep}{image}",
                                              f"{self.B_pth}{os.path.sep}{image}",
                                              f"{self.AB_pth}{os.path.sep}val{os.path.sep}{image}")
            if i % 50 == 0:
                print(f"{i}/{n_examples} placed")


    @staticmethod
    def create_FID_dataset(root_pth, results_pth, model):
        """ creates a dataset used to evaluate model

            creates a directory of the following structure
            - root_pth
                - fake
                - real

            root_pth: root directory
            results_pth: directory containing test results (generated imgs).
                         used for the fake images directory described above.

            model: pix2pix or bicyclegan or none
        """
        if os.path.exists(root_pth):
            shutil.rmtree(root_pth)

        os.mkdir(root_pth)

        fake_pth = f"{root_pth}{os.path.sep}fake"
        real_pth = f"{root_pth}{os.path.sep}real"

        os.mkdir(fake_pth)
        os.mkdir(real_pth)

        count = 0
        for i, sample in enumerate(os.listdir(results_pth)):
            if sample[10] == 'r' and model == "bicyclegan":
                shutil.copy(f"{results_pth}{os.path.sep}{sample}",
                            f"{fake_pth}{os.path.sep}{count}.png")
                count += 1



            elif model == 'pix2pix' and i % 3 == 0:
                if sample.count("fake"):
                    shutil.copy(f"{results_pth}{os.path.sep}{sample}",
                                f"{fake_pth}{os.path.sep}{count}.png")
                count += 1


            elif model is None:
                img = cv2.imread(f"{results_pth}{os.path.sep}{sample}")
                img = cv2.resize(img, (256, 256))
                cv2.imwrite(f"{fake_pth}{os.path.sep}{count}.png", img)

                count += 1

            print("val folder:" + str(count))
        with open(os.path.join(DATA_ROOT, METADATA)) as json_file:

            data = json.load(json_file)
            for i, model in enumerate(data):
                print(i)
                if i == count:
                    break

                im = Image.open(f"{PARENT_DATASET}{os.path.sep}{model['model_id']}{os.path.sep}texture.png").convert("RGB")
                im = MaterialPainter.scale_img(256, im)
                if im.size != (256,256):
                    continue

                im.save(f"{real_pth}{os.path.sep}{i}.png")



ds = Dataset({"Wood", "Smooth Leather", "Composite Board", "Rough Cloth"}, BLANK_DATASET, TEXTURED_DATASET, AB_LOC)
# 271.6 seconds
# ds.create_blank_dataset(False)
# ds.create_AB_dataset()
# ds.create_dataset_parallel()

# 90 seconds

ds.create_FID_dataset("/home/micah/year3/dissertation/src/project_src/webapp/evaluation/FID",
                     "/home/micah/year3/dissertation/src/BicycleGAN/results/textures/val/images", "bicyclegan")
# FID
# lambda 10 - 105.182
# lambda 5 - 105.5655474767783
# lambda 15 - 110

# ef 32 - 107.67
# ef 100