import numpy as np
import matplotlib.pyplot as plt
import os
import json
import categories
from sklearn.manifold import TSNE
import pandas as pd
import os
import matplotlib.font_manager

DATA_ROOT = "/home/micah/year3/dissertation/datasets/3D-FUTURE-model"
METADATA = os.path.join(DATA_ROOT, "model_info.json")


class DatasetAnalysis:

    def __init__(self, cat):
        self.category_list = categories._ATTR_MATERIAL + categories._ATTR_STYLE + categories._SUPER_CATEGORIES_3D
        self.categories = cat
        self.metadata = os.path.join(DATA_ROOT, "model_info.json")
        #self.analyse_classes()
        #self.ds_count()

    @staticmethod
    def generate_bar_chart(d, attribute_type, super_category):
        x = []
        y = []
        for i, key in enumerate(d):
            y.append(d[key])
            x.append(key)

        plt.bar(x, y, 0.8)
        plt.xticks(rotation=90)
        plt.gcf().subplots_adjust(bottom=0.38)
        plt.title(f"{attribute_type} attribute distribution with {super_category}")
        plt.ylabel("Instances")
        plt.show()




    def analyse_classes(self):
        category_table = {}
        theme_table = {}
        material_table = {}
        style_table = {}
        attributes = ['style', 'theme', 'material']
        tables = [style_table, theme_table, material_table]

        with open(self.metadata) as json_file:
            data = json.load(json_file)

            for key in self.categories:
                for model in data:
                    if model['super-category'] == key:
                        for i, attr in enumerate(attributes):
                            if not model[attr] in tables[i]:
                                tables[i][model[attr]] = 1

                            elif model[attr] in tables[i]:
                                tables[i][model[attr]] += 1
                #
                # self.generate_bar_chart(theme_table, "Theme", key)
                # self.generate_bar_chart(material_table, "Material", key)
                # self.generate_bar_chart(style_table, "Style", key)

        # print(theme_table)
        # print(style_table)
        print(material_table)
        json_file.close()


    def ds_count(self):
        material_attr = {}
        theme_attr = {}
        with open(self.metadata) as json_file:
            data = json.load(json_file)
            style_attr = {"Light Luxury"}
            material_attr = {"Wood":1, "Composite Board":1, "Composition":1, "Solid Wood":1, "Metal":1, "Marble":1}
            count = 0
            for model in data:

                if not model['theme'] in theme_attr:
                    theme_attr[model['theme']] = 1
                else:
                    theme_attr[model['theme']] += 1

                if not model['material'] in material_attr:
                    material_attr[model['material']] = 1

                else:
                    material_attr[model['material']] += 1

        material_vals = [(k, material_attr[k]) for k in material_attr]
        theme_vals = [(k, theme_attr[k]) for k in theme_attr]
        theme_vals.sort(key=self.sort_by_freq, reverse=True)
        material_vals.sort(key=self.sort_by_freq, reverse=True)

        print(theme_vals)
        print(material_vals)


    def ds_count_constraint(self):
        # themes = {"Smooth Net", "Texture Mark", "Gold Foil"}
        materials = {"Wood", "Smooth Leather", "Composite Board", "Rough Cloth", "Suede"}
        with open(self.metadata) as json_file:
            data = json.load(json_file)
            count = 0
            for model in data:
                if model['material'] in materials:
                    count += 1
                    print(model['model_id'])
            print(count)

    def sort_by_freq(self, attr):
        return attr[1]

    def string_to_num(self, text):
        s = ""
        for i in s:
            s += str(ord(i))
        return s

    def resolve_value(self, attr):
        for attr_type in self.category_list:
            if attr is not None and attr in attr_type['category']:
                return attr_type['type']

        return 20

    def tsne_vis(self):

        # df = pd.read_json(self.metadata)
        # df.pop("model_id")
        # df.pop("theme")
        # df.pop("category")
        #
        # for idx, row in df.iterrows():
        #     df.loc[idx, 'material'] = self.resolve_value(df.loc[idx, 'material'])
        #     df.loc[idx, 'style'] = self.resolve_value(df.loc[idx, 'style'])
        #     df.loc[idx, 'super-category'] = self.resolve_value(df.loc[idx, 'super-category'])
        #
        # df.to_csv("attr_data.csv", sep=",", index=False)
        df = pd.read_csv("attr_data.csv")
        # # df['material'] = df['material'].apply(self.resolve_value)
        # df['material'] = self.string_to_num(df['material'])
        # d
        print(df['material'])
        # print(df)
        # print(df.shape)
        # df['material'] = pd.to_numeric(df['material'], errors='coerce')
        # df['theme'] = pd.to_numeric(df['theme'], errors='coerce')
        # df['super-category'] = pd.to_numeric(df['super-category'], errors='coerce')
        # df['style'] = pd.to_numeric(df['style'], errors='coerce')

        # print(df['material'])
        # df.apply(pd.to_numeric)
        print(df)
        from sklearn.manifold import TSNE
        tsne_em = TSNE(n_components=2, perplexity=30.0, n_iter=1000, verbose=1).fit_transform(df)
        from bioinfokit.visuz import cluster
        cluster.tsneplot(score=tsne_em)
        color_class = df['material'].to_numpy()
        cluster.tsneplot(score=tsne_em, colorlist=color_class, legendpos='upper right', legendanchor=(1.15, 1))


analysis = DatasetAnalysis({'Cabinet/Shelf/Desk': None, 'Table': None, 'Chair': None, 'Bed':None, "Sofa": None, 'Pier/Stool':None})
analysis.ds_count_constraint()
# analysis.tsne_vis()
analysis.ds_count()