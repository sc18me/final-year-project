## Installation 

Clone the repo and then cd into the project's root directory.

```git clone git@gitlab.com:sc18me/final-year-project.git```

```cd final-year-project/```

Install dependencies.

```./install.sh```

Launch the web application.

```./texture_generator```

The web application can then be accessed via http://127.0.0.1:5000/ 


## Tips
Use plenty of vectors if you want to specify orientation. *Note* this feature will only work for anisotropic textures (wood, composite). 
Generating textures without user guided orientation may give better results.
